import React from 'react';
import ReactDOM from 'react-dom/client';
import { ConfigProvider } from 'antd';
import arEG from 'antd/lib/locale/ar_EG';
import './index.css';
import App from './App';


ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <ConfigProvider direction="rtl" locale={arEG}>
      <App />
    </ConfigProvider>,
   </React.StrictMode>
);

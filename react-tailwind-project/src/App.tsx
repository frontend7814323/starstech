import React from 'react';
import ContractsPage from './pages/ContractsPage';

const App: React.FC = () => {
  return (
    <div className="bg-aliceblue min-h-screen">
      <ContractsPage />
    </div>
  );
};

export default App;

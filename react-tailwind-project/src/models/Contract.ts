export interface Contract {
    id: string;
    name: string;
    description: string;
    status: string;
    content: string;
  }
  
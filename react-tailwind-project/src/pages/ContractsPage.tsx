import React, { useState, useEffect } from 'react';
import { Button, message, Modal, Spin } from 'antd';
import { Contract } from '../models/Contract';
import ContractsTable from '../components/ContractTable';
import ContractForm from '../components/ContractForm';
import axios from 'axios';

const API_URL = 'https://66645669932baf9032aac22e.mockapi.io/api/v1/contract';

const ContractsPage: React.FC = () => {
  const [contracts, setContracts] = useState<Contract[]>([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editingContract, setEditingContract] = useState<Contract | undefined>(undefined);
  const [statusOptions, setStatusOptions] = useState<string[]>([]);
  const [deleteConfirmationVisible, setDeleteConfirmationVisible] = useState(false);
  const [contractToDeleteId, setContractToDeleteId] = useState<string>('');
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    fetchContracts();
    fetchStatusOptions();
  }, []);

  const fetchContracts = async () => {
    setIsLoading(true);
    try {
      const response = await axios.get(API_URL);
      setContracts(response.data);
    } catch (error) {
      message.error('Failed to fetch contracts');
    } finally {
      setIsLoading(false);
    }
  };

  const fetchStatusOptions = async () => {
    // Replace this with the actual API call to get status options if applicable
    setStatusOptions(['Active', 'Inactive']);
  };

  const handleAddContract = async (contract: Contract) => {
    setIsSubmitting(true);
    try {
      const response = await axios.post(API_URL, contract);
      setContracts([...contracts, response.data]);
      message.success('Contract added successfully');
    } catch (error) {
      message.error('Failed to add contract');
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleEditContract = async (updatedContract: Contract) => {
    setIsSubmitting(true);
    try {
      const response = await axios.put(`${API_URL}/${updatedContract.id}`, updatedContract);
      setContracts(contracts.map((contract) => (contract.id === updatedContract.id ? response.data : contract)));
      message.success('Contract updated successfully');
    } catch (error) {
      message.error('Failed to update contract');
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleDeleteContract = async (id: string) => {
    setIsLoadingDelete(true);
    try {
      await axios.delete(`${API_URL}/${id}`);
      setContracts(contracts.filter((contract) => contract.id !== id));
      message.success('Contract deleted successfully');
    } catch (error) {
      message.error('Failed to delete contract');
    } finally {
      setIsLoadingDelete(false);
    }
  };

  const confirmDelete = (id: string) => {
    setDeleteConfirmationVisible(true);
    setContractToDeleteId(id);
  };

  const handleConfirmDelete = async () => {
    if (contractToDeleteId) {
      await handleDeleteContract(contractToDeleteId);
      setDeleteConfirmationVisible(false);
    }
  };

  const openEditModal = (contract: Contract) => {
    // console.log(contract);
    setEditingContract(contract);
    setIsModalVisible(true);
  };

  const openCreateModal = () => {
    setIsModalVisible(true);
    setEditingContract(undefined); 
  };

  const handleFormSubmit = (contract: Contract) => {
    if (editingContract) {
      handleEditContract(contract);
    } else {
      handleAddContract(contract);
    }
    setIsModalVisible(false);
    setEditingContract(undefined);
  };

  return (
    <div className="container mx-auto p-24" style={{ padding: "100px" }} dir="rtl">
      <div className="flex justify-between mb-4">
        <h1 className="text-xl font-bold">نماذج العقود</h1>
        <Button type="primary" onClick={openCreateModal}>إنشاء نموذج</Button>
      </div>

      {isLoading ? (
        <Spin size="large" />
      ) : (
        <ContractsTable contracts={contracts} onEdit={openEditModal} onDelete={confirmDelete} />
      )}
      <ContractForm
        visible={isModalVisible}
        onClose={() => {
          setIsModalVisible(false);
          setEditingContract(undefined);
        }}
        onSubmit={handleFormSubmit}
        initialValues={editingContract} 
        statusOptions={statusOptions}
        isEditing={!!editingContract} 
        isSubmitting={isSubmitting} 
      />

      {/* Delete confirmation modal */}
      <Modal
        title="تأكيد الحذف"
        open={deleteConfirmationVisible}
        onOk={handleConfirmDelete}
        onCancel={() => setDeleteConfirmationVisible(false)}
        okType="danger"
        confirmLoading={isLoadingDelete}
      width={550}
      >
        <p>هل أنت متأكد أنك تريد حذف هذا العقد؟</p>
      </Modal>
    </div>
  );
};

export default ContractsPage;

import React, { useState } from 'react';
import { Table, Button, ConfigProvider, Space, Modal } from 'antd';
import { Contract } from '../models/Contract';
import arEG from 'antd/lib/locale/ar_EG';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';

interface ContractsTableProps {
  contracts: Contract[];
  onEdit: (contract: Contract) => void;
  onDelete: (id: string) => void;
}

const ContractsTable: React.FC<ContractsTableProps> = ({ contracts, onEdit, onDelete }) => {
  const [modalContent, setModalContent] = useState<string>('');

  const columns = [
    {
      title: 'اسم النموذج',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'وصف النموذج',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'الحالة',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'محتوى النموذج',
      dataIndex: 'content',
      key: 'content',
      render: (text: string) => <Button onClick={() => setModalContent(text)}>عرض</Button>,
    },
    {
      title: 'الإجراءات',
      key: 'actions',
      render: (text: string, record: Contract) => (
        <Space>
          <Button icon={<EditOutlined />} onClick={() => onEdit(record)} />
          <Button icon={<DeleteOutlined />} onClick={() => onDelete(record.id)} danger type="primary" />
        </Space>
      ),
    },
  ];

  return (
    <ConfigProvider direction="rtl" locale={arEG}>
      <div style={{ overflowX: 'auto' }}>
        <Table
          dataSource={contracts}
          columns={columns}
          rowKey="id"
          pagination={{ position: ['bottomCenter'] }}
          style={{ minWidth: '600px', textAlign: 'center' }}
        />
      </div>
      <Modal
        title="عرض محتوى النموذج"
        visible={!!modalContent}
        onCancel={() => setModalContent('')}
        footer={null}
      >
        <div dangerouslySetInnerHTML={{ __html: modalContent }} />
      </Modal>
    </ConfigProvider>
  );
};

export default ContractsTable;

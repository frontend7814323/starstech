import React, { useEffect, useRef } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Contract } from '../models/Contract';
import { Modal, Button, Input, Select } from 'antd';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const { Option } = Select;

interface ContractFormProps {
  visible: boolean;
  onClose: () => void;
  onSubmit: (data: Contract) => void;
  initialValues?: Contract;
  statusOptions: string[];
  isEditing: boolean;
  isSubmitting: boolean;
}

const ContractForm: React.FC<ContractFormProps> = ({
  visible,
  onClose,
  onSubmit,
  initialValues,
  statusOptions,
  isEditing,
  isSubmitting, 
}) => {
  const { control, handleSubmit, setValue, reset } = useForm<Contract>({
    defaultValues: {
      id: '',
      name: '',
      description: '',
      status: '',
      content: '',
    },
  });

  const quillRef = useRef<ReactQuill>(null);

  useEffect(() => {
    const resetFormValues = () => {
      const defaultValues = {
        id: '',
        name: '',
        description: '',
        status: '',
        content: '',
      };
      reset(initialValues && isEditing ? initialValues : defaultValues);
    };

    resetFormValues();
  }, [initialValues, reset, isEditing]);

  useEffect(() => {
    if (initialValues) {
      setValue('name', initialValues.name || '');
      setValue('description', initialValues.description || '');
      setValue('status', initialValues.status || '');
      setValue('content', initialValues.content || '');
    }
  }, [initialValues, setValue]);

  const handleFormSubmit = (formData: Contract) => {
    // console.log("Added data:", formData);
    onSubmit(formData);
    onClose();
  };

  const handleEdit = (field, value) => {
    // console.log("Edited data:", { [field]: value });
  };

  return (
    <Modal
      title={isEditing ? 'تعديل نموذج عقد' : 'إنشاء نموذج عقد'}
      open={visible}
      onCancel={onClose}
      footer={null}
      className="rtl-modal"
      width={550}
      centered
    >
      <form onSubmit={handleSubmit(handleFormSubmit)} dir="rtl" className="contract-form p-4">
        <div className="mb-4">
          <label className="block mb-2 font-bold">اسم النموذج</label>
          <Controller
            name="name"
            control={control}
            render={({ field }) => (
              <Input
                {...field}
                onChange={(e) => {
                  field.onChange(e);
                  handleEdit('name', e.target.value);
                }}
              />
            )}
          />
        </div>
        <div className="mb-4">
          <label className="block mb-2 font-bold">وصف النموذج</label>
          <Controller
            name="description"
            control={control}
            render={({ field }) => (
              <Input
                {...field}
                onChange={(e) => {
                  field.onChange(e);
                  handleEdit('description', e.target.value);
                }}
              />
            )}
          />
        </div>
        <div className="mb-4">
          <label className="block mb-2 font-bold"> الحالة</label>
          <Controller
            name="status"
            control={control}
            render={({ field }) => (
              <Select
                {...field}
                onChange={(value) => {
                  field.onChange(value);
                  handleEdit('status', value);
                }}
                className="w-full" 
              >
                {statusOptions.map((status) => (
                  <Option key={status} value={status}>
                    {status}
                  </Option>
                ))}
              </Select>
            )}
          />
        </div> 
        <div className="mb-4 h-[200px]">
          <label className="block mb-2 font-bold">محتوى النموذج</label>
          <Controller
            name="content"
            control={control}
            render={({ field }) => (
              <ReactQuill
                {...field}
                theme="snow"
                onChange={(value) => {
                  field.onChange(value);
                  handleEdit('content', value);
                }}
                className="resizable-textarea"
                modules={{
                  toolbar: [
                    [{ header: [1, 2, false] }],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{ list: 'ordered' }, { list: 'bullet' }],
                    ['link', 'image'],
                    ['clean'],
                  ],
                }}
                formats={[
                  'header',
                  'bold',
                  'italic',
                  'underline',
                  'strike',
                  'list',
                  'bullet',
                  'link',
                  'image',
                ]}
                style={{ height: '10em' }}
                ref={quillRef}
              />
            )}
          />
        </div>
        <div className="flex justify-end mt-6">
          <Button type="primary" htmlType="submit" loading={isSubmitting}>
            {isEditing ? 'تحديث' : 'حفظ'}
          </Button>
          <div className="w-4"></div>
          <Button onClick={onClose}>إلغاء</Button>
        </div>
      </form>
    </Modal>
  );
};

export default ContractForm;
